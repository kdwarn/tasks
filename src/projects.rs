use std::collections::HashMap;
use std::fmt::{self, Display, Formatter};

use autosurgeon::{hydrate, Hydrate, Reconcile};
use chrono::{DateTime, Utc};
use iced::{
    widget::{button, column, row, text},
    Alignment, Element,
};
use uuid::Uuid;

use crate::{notes::NoteType, tasks::Priority, App, Msg, TaskDoc};

#[derive(Debug, Clone, Reconcile, Hydrate, PartialEq)]
pub struct Project {
    pub name: String,
    pub due: Option<i64>,
    pub description: String,
    pub priority: Priority,
    pub completed: bool,
    pub tags: Vec<String>,
}

impl Default for Project {
    fn default() -> Self {
        Self {
            name: String::new(),
            due: None,
            description: String::new(),
            priority: Priority::Normal,
            completed: false,
            tags: vec![],
        }
    }
}

impl Display for Project {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.name)
    }
}

/// Get projects.
pub fn get_projects(app: &App) -> HashMap<Uuid, Project> {
    let doc: TaskDoc = hydrate(&app.doc).unwrap();
    doc.projects
}

/// Get one project.
pub fn get(app: &App, id: Uuid) -> Option<Project> {
    let doc: TaskDoc = hydrate(&app.doc).unwrap();
    doc.projects.get(&id).cloned()
}

/// Check if a project's name already exists.
pub fn project_name_exists(app: &App, id: Uuid) -> bool {
    get_projects(app)
        .iter()
        .any(|(project_id, project)| project.name == app.project.name && project_id != &id)
}

/// Get and display project details.
pub fn project_meta(app: &App, id: Uuid) -> Element<Msg> {
    let project = get(app, id).unwrap();
    let name = row![text(project.name)];
    let description = row![text(project.description)];

    let status = if project.completed {
        "Completed".to_string()
    } else {
        "Incomplete".to_string()
    };

    let due = match project.due {
        Some(v) => {
            let dt: DateTime<Utc> =
                DateTime::<Utc>::from_timestamp(v, 0).expect("invalid timestamp");
            format!("{:?}", dt)
        }
        None => "No due date".to_string(),
    };

    let tags = if project.tags.is_empty() {
        "No tags.".to_string()
    } else {
        project.tags.join(", ")
    };
    column![
        row![text("PROJECT: "), name],
        row![text("Description:"), description]
            .align_items(Alignment::Center)
            .spacing(8),
        row![text("Due:"), text(due)]
            .align_items(Alignment::Center)
            .spacing(8),
        row![text("Tagged:"), text(tags)]
            .align_items(Alignment::Center)
            .spacing(8),
        row![text("Priority:"), text(project.priority)]
            .align_items(Alignment::Center)
            .spacing(8),
        row![text("Status:"), text(status)]
            .align_items(Alignment::Center)
            .spacing(8),
    ]
    .spacing(10)
    .into()
}

pub fn project_controls<'a>(id: Uuid) -> Element<'a, Msg> {
    row![
        button("Edit Project").on_press(Msg::EditProject(id)),
        button("Delete Project").on_press(Msg::ConfirmDeleteProject(id)),
        button("Add Note").on_press(Msg::CreateNote(NoteType::Project, Some(id)))
    ]
    .spacing(10)
    .into()
}
