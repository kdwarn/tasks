use std::collections::{HashMap, VecDeque};
use std::fs::{self, File, OpenOptions};
use std::io::Write;

use automerge::{transaction::Transactable, AutoCommit, AutoSerde, ObjType};
use autosurgeon::{hydrate, reconcile, Hydrate, Reconcile};
use chrono::{DateTime, Utc};
use iced::{
    color, executor, theme,
    widget::{
        button, column, container, row, scrollable, text, text_editor, text_input, vertical_rule,
        TextInput,
    },
    Alignment, Application, Command, Element, Length, Settings, Theme,
};
use uuid::Uuid;

mod notes;
mod projects;
mod styles;
mod tasks;
use notes::*;
use projects::*;
use styles::*;
use tasks::*;

// Ids used for focusing text_input areas
const PROJECT_NAME_INPUT_ID: &str = "project_name";
const TASK_NAME_INPUT_ID: &str = "task_name";
const NOTE_INPUT_ID: &str = "project_note";

pub fn main() -> iced::Result {
    App::run(Settings::default())
}

type ProjectId = Uuid;
type TaskId = Uuid;
type ParentId = Uuid;
type NoteId = Uuid;

/// Actions the user/program can do.
#[derive(Debug, Clone)]
pub enum Msg {
    // Project-level
    CreateProject,
    ViewProject(ProjectId),
    EditProject(ProjectId),
    SaveProject(ProjectId),
    ConfirmDeleteProject(ProjectId),
    DeleteProject(ProjectId),
    // Task-level
    ViewTask(TaskId),
    EditTask(TaskId),
    ChangeCompletedStatus(TaskId, bool),
    ChangePriority(Priority),
    SaveTask(TaskId, Option<ProjectId>),
    DeleteTask(TaskId),
    FilterTasks(TaskFilter),
    SortTasks(TaskSort),
    SelectTask,
    // Note-level
    CreateNote(NoteType, Option<ParentId>),
    EditNote(NoteId, Note),
    ChangeNoteText(text_editor::Action),
    SaveNote(NoteType, NoteId, Option<ParentId>),
    DeleteNote(NoteId),
    FilterNotes(Option<NoteType>),
    SelectNote,
    // Other
    Home,
    IndependentTasks,
    TextInputChanged(Field, String),
    Cancel,
}

// The state that the UI view is in.
#[derive(Copy, Clone, Debug)]
pub enum View {
    AllTasks, // Default, "home" view
    IndependentTasks,
    ProjectDetail(Op, ProjectId),
    TaskDetail(Op, TaskId),
    NoteDetail(Op, NoteType, NoteId, Option<ParentId>),
}

impl View {
    fn title(&self) -> String {
        String::from(match self {
            View::AllTasks => "All tasks",
            View::IndependentTasks => "Independent tasks",
            View::ProjectDetail(_, _) => "Project",
            View::TaskDetail(_, _) => "Task",
            View::NoteDetail(_, _, _, _) => "Note",
        })
    }
}

/// Common operations on the various types
#[derive(Copy, Clone, Debug)]
pub enum Op {
    Create,
    View,
    Edit,
    Delete,
}

// The field that a changing text_input is for.
#[derive(Copy, Clone, Debug)]
pub enum Field {
    ProjectName,
    ProjectDescription,
    TaskName,
    TaskDescription,
}

pub struct App {
    doc: AutoCommit,
    view: VecDeque<View>,
    message_to_user: String,
    project: Project, // a new project ready to be created or current one being acted on
    backup_project: Project, // a version of the current project prior to any existing changes
    task: Task,       // new or current task
    backup_task: Task,
    task_filter: TaskFilter,
    task_sort: TaskSort,
    note_filter: Option<NoteType>,
    note_text: text_editor::Content,
}

impl Application for App {
    type Message = Msg;
    type Executor = executor::Default;
    type Flags = ();
    type Theme = Theme;

    fn new(_flags: Self::Flags) -> (Self, Command<Self::Message>) {
        // Open/create file, load/create Autocommit doc.
        let doc = match fs::read("tasks") {
            Ok(v) => AutoCommit::load(&v).unwrap(),
            Err(_) => {
                let mut f = File::create("tasks").unwrap();
                let mut doc = AutoCommit::new();
                doc.put_object(automerge::ROOT, "projects", ObjType::Map)
                    .unwrap();
                doc.put_object(automerge::ROOT, "tasks", ObjType::Map)
                    .unwrap();
                doc.put_object(automerge::ROOT, "notes", ObjType::Map)
                    .unwrap();
                f.write_all(&doc.save()).unwrap();
                doc
            }
        };

        let project = Project::default();
        let task = Task::default();

        (
            Self {
                doc,
                view: VecDeque::from(vec![View::AllTasks]),
                message_to_user: String::new(),
                project: project.clone(),
                backup_project: project,
                task: task.clone(),
                backup_task: task,
                task_filter: TaskFilter::Incomplete,
                task_sort: TaskSort::Due,
                note_filter: None,
                note_text: text_editor::Content::new(),
            },
            // various Commands to initialize the app
            Command::batch([
                text_input::focus(text_input::Id::new(TASK_NAME_INPUT_ID)),
                iced::window::maximize(true),
            ]),
        )
    }

    fn update(&mut self, msg: Msg) -> Command<Self::Message> {
        // Clear any previously set message to user.
        self.message_to_user = "".to_string();

        // NOTE: anytime a text input is changed, the field is automatically updated, and
        // so anywhere that can happen, the self.project/task needs to be cloned and put into
        // self.backup_project/task so they can be swapped back if cancelled/navigated away
        // from without saving (which is currently being done in Msg::Cancel).

        match msg {
            // Project-level
            Msg::CreateProject => {
                self.project = Project::default();
                self.view
                    .push_front(View::ProjectDetail(Op::Create, Uuid::new_v4()));
                text_input::focus(text_input::Id::new(PROJECT_NAME_INPUT_ID))
            }
            Msg::ViewProject(id) => {
                self.task = Task::default();
                self.note_filter = None;
                self.view.push_front(View::ProjectDetail(Op::View, id));
                text_input::focus(text_input::Id::new(TASK_NAME_INPUT_ID))
            }
            Msg::EditProject(id) => {
                let project = projects::get(self, id).unwrap();
                self.project = project.clone();
                self.backup_project = project;
                self.view.push_front(View::ProjectDetail(Op::Edit, id));
                text_input::focus(text_input::Id::new(PROJECT_NAME_INPUT_ID))
            }
            Msg::SaveProject(id) => {
                if self.project.name.is_empty() {
                    self.message_to_user = "Project name cannot be blank.".to_string();
                } else if project_name_exists(self, id) {
                    self.message_to_user = "Project name already in use.".to_string();
                } else {
                    let mut doc: TaskDoc = hydrate(&self.doc).unwrap();

                    // Insert new Project or update existing Project
                    let project = doc.projects.entry(id).or_default();
                    *project = self.project.clone();

                    self.write_to_file(doc);
                    self.message_to_user = "Project saved.".to_string();
                    // On success, direct to view (while in the other cases view stays the same).
                    self.view.push_front(View::ProjectDetail(Op::View, id));
                }
                text_input::focus(text_input::Id::new(TASK_NAME_INPUT_ID))
            }
            Msg::ConfirmDeleteProject(id) => {
                self.view.push_front(View::ProjectDetail(Op::Delete, id));
                Command::none()
            }
            Msg::DeleteProject(id) => {
                let mut doc: TaskDoc = hydrate(&self.doc).unwrap();

                // Get all tasks associated with project.
                let mut tasks = get_tasks(self);
                tasks.retain(|_, task| task.project_id.is_some_and(|project_id| project_id == id));

                // Get only ids of the those tasks.
                let task_ids = tasks.keys().collect::<Vec<_>>();

                // Collect all notes (task- and project-level) to delete.
                let mut note_ids_to_delete = vec![];
                for (note_id, note) in &doc.notes {
                    if let Some(v) = note.parent_id {
                        if v == id || task_ids.contains(&&v) {
                            note_ids_to_delete.push(*note_id);
                        }
                    }
                }

                // Delete all notes.
                for note_id in note_ids_to_delete {
                    doc.notes.remove(&note_id);
                }

                // Delete the tasks.
                for task_id in task_ids {
                    doc.tasks.remove(task_id);
                }

                // Delete the project.
                doc.projects.remove(&id);
                self.write_to_file(doc);
                self.view.push_front(View::AllTasks);
                Command::none()
            }

            // Task-level
            Msg::FilterTasks(filter) => {
                match filter {
                    TaskFilter::All => self.task_filter = TaskFilter::All,
                    TaskFilter::Incomplete => self.task_filter = TaskFilter::Incomplete,
                    TaskFilter::Completed => self.task_filter = TaskFilter::Completed,
                };
                Command::none()
            }
            Msg::SortTasks(sort) => {
                match sort {
                    TaskSort::Priority => self.task_sort = TaskSort::Priority,
                    TaskSort::Due => self.task_sort = TaskSort::Due,
                    TaskSort::Created => self.task_sort = TaskSort::Created,
                }
                Command::none()
            }
            Msg::DeleteTask(id) => {
                let mut doc: TaskDoc = hydrate(&self.doc).unwrap();

                // Collect all notes to delete.
                let mut note_ids_to_delete = vec![];
                for (note_id, note) in &doc.notes {
                    if let Some(v) = note.parent_id {
                        if v == id {
                            note_ids_to_delete.push(*note_id);
                        }
                    }
                }

                // Delete all notes.
                for note_id in note_ids_to_delete {
                    doc.notes.remove(&note_id);
                }

                // Delete the task.
                doc.tasks.remove(&id);
                self.write_to_file(doc);

                self.task = Task::default();

                /*
                The Delete button for a task can be clicked from:
                 - AllTasks
                 - ProjectDetail(Op::View)
                 - TaskDetail(Op::View)

                Returning to the first two is straightforward, but for TaskDetail(Op::View), the
                task no longer exists, so we have to go to a different view - either:
                 - AllTasks if user had clicked on the task from there and then deleted
                 - ProjectDetail if user had clicked on the task from there and then deleted
                */

                match self.view.get(0) {
                    Some(View::AllTasks) => self.view.push_front(View::AllTasks),
                    Some(View::ProjectDetail(_, id)) => {
                        self.view.push_front(View::ProjectDetail(Op::View, *id))
                    }
                    Some(View::TaskDetail(Op::View, _)) => match self.view.get(1) {
                        Some(View::AllTasks) => self.view.push_front(View::AllTasks),
                        Some(View::ProjectDetail(op, id)) => {
                            self.view.push_front(View::ProjectDetail(*op, *id))
                        }
                        _ => self.view.push_front(View::AllTasks),
                    },
                    // Anything else should not be a possibility; just go home.
                    _ => self.view.push_front(View::AllTasks),
                }
                Command::none()
            }
            Msg::ChangeCompletedStatus(task_id, status) => {
                let mut doc: TaskDoc = hydrate(&self.doc).unwrap();

                let task = doc.tasks.get_mut(&task_id).unwrap();
                task.completed = status;

                // If currently viewing or editing a task, update its status
                match self.view.get(0) {
                    Some(View::TaskDetail(Op::View, _)) | Some(View::TaskDetail(Op::Edit, _)) => {
                        self.task.completed = status;
                    }
                    _ => (),
                };

                self.write_to_file(doc);
                Command::none()
            }
            Msg::ChangePriority(priority) => {
                self.task.priority = priority;
                Command::none()
            }
            Msg::ViewTask(id) => {
                let task = tasks::get(self, id).unwrap();
                self.task = task;
                self.note_filter = None;
                self.view.push_front(View::TaskDetail(Op::View, id));
                text_input::focus(text_input::Id::new(TASK_NAME_INPUT_ID))
            }
            Msg::EditTask(id) => {
                let task = tasks::get(self, id).unwrap();
                self.task = task.clone();
                self.backup_task = task;
                self.view.push_front(View::TaskDetail(Op::Edit, id));
                text_input::focus(text_input::Id::new(TASK_NAME_INPUT_ID))
            }
            Msg::SaveTask(task_id, project_id) => {
                if self.task.name.is_empty() {
                    self.message_to_user = "Task name cannot be blank.".to_string();
                } else {
                    let mut doc: TaskDoc = hydrate(&self.doc).unwrap();

                    // Potentially add the project this task belongs to.
                    self.task.project_id = project_id;

                    let task = doc.tasks.entry(task_id).or_default();
                    *task = self.task.clone();

                    // Set created/updated date.
                    if task.created.is_none() {
                        task.created = Some(Utc::now().timestamp());
                    }

                    task.edited = Some(Utc::now().timestamp());

                    self.write_to_file(doc);

                    /*
                    A task can be saved from:
                     - AllTasks or ProjectDetail(Op::View)  - task created
                     - TaskDetail(Op::Edit) - task edited

                    When creating a task, we want to return to the same view, so nothing
                    needs to get done with view.
                    */

                    // Go back one view if editing a task.
                    if let Some(View::TaskDetail(Op::Edit, _)) = self.view.get(0) {
                        self.view.pop_front();
                    }

                    // If that takes us to viewing a task, go back one more view.
                    if let Some(View::TaskDetail(Op::View, _)) = self.view.get(0) {
                        self.view.pop_front();
                    }

                    // Always reset the Task.
                    self.task = Task::default();
                }
                Command::none()
            }
            Msg::SelectTask => Command::none(),
            Msg::CreateNote(note_type, parent_id) => {
                self.note_text = text_editor::Content::new();
                self.view.push_front(View::NoteDetail(
                    Op::Create,
                    note_type,
                    Uuid::new_v4(),
                    parent_id,
                ));
                text_input::focus(text_input::Id::new(NOTE_INPUT_ID))
            }
            Msg::EditNote(id, note) => {
                self.note_text = text_editor::Content::with_text(&note.text);
                self.view.push_front(View::NoteDetail(
                    Op::Edit,
                    note.note_type,
                    id,
                    note.parent_id,
                ));
                text_input::focus(text_input::Id::new(NOTE_INPUT_ID))
            }
            Msg::ChangeNoteText(action) => {
                self.note_text.perform(action);
                Command::none()
            }
            Msg::SaveNote(note_type, note_id, parent_id) => {
                // Convert note's text from Content to String.
                let text = self.note_text.text().trim().to_string();

                // Return if empty note text.
                if text.is_empty() {
                    self.message_to_user = "Note cannot be blank.".to_string();
                    return Command::none();
                }

                let mut doc: TaskDoc = hydrate(&self.doc).unwrap();

                // Insert new Note or update existing Note
                let note = doc.notes.entry(note_id).or_default();
                note.text = text;
                note.note_type = note_type;
                note.parent_id = parent_id;

                // Set created/updated date.
                if note.created.is_none() {
                    note.created = Some(Utc::now().timestamp());
                }

                note.edited = Some(Utc::now().timestamp());

                // Set view.
                // If user added (or edited) an App-level Note, return to AllTasks, otherwise
                // to view before last (where they clicked on edit or add button).
                match note_type {
                    NoteType::App => {
                        self.view.push_front(View::AllTasks);
                    }
                    NoteType::Project => {
                        self.view.pop_front();
                    }
                    NoteType::Task => {
                        self.view.pop_front();
                    }
                }
                self.message_to_user = String::from("Note saved.");
                self.write_to_file(doc);
                Command::none()
            }
            Msg::DeleteNote(note_id) => {
                let mut doc: TaskDoc = hydrate(&self.doc).unwrap();
                doc.notes.remove(&note_id);
                self.write_to_file(doc);
                Command::none()
            }
            Msg::FilterNotes(filter) => {
                match filter {
                    None => self.note_filter = None,
                    Some(NoteType::App) => self.note_filter = Some(NoteType::App),
                    Some(NoteType::Project) => self.note_filter = Some(NoteType::Project),
                    Some(NoteType::Task) => self.note_filter = Some(NoteType::Task),
                };
                Command::none()
            }
            Msg::SelectNote => Command::none(),
            Msg::Home => {
                // Reset the UI.
                self.task_filter = TaskFilter::Incomplete;
                self.note_filter = None;
                self.project = Project::default();
                self.task = Task::default();
                self.view.push_front(View::AllTasks);
                text_input::focus(text_input::Id::new(TASK_NAME_INPUT_ID))
            }
            Msg::IndependentTasks => {
                // Reset the UI.
                self.task_filter = TaskFilter::Incomplete;
                self.note_filter = None;
                self.project = Project::default();
                self.task = Task::default();
                self.view.push_front(View::IndependentTasks);
                text_input::focus(text_input::Id::new(TASK_NAME_INPUT_ID))
            }
            Msg::TextInputChanged(field, value) => {
                match field {
                    Field::ProjectName => self.project.name = value,
                    Field::ProjectDescription => self.project.description = value,
                    Field::TaskName => self.task.name = value,
                    Field::TaskDescription => self.task.description = value,
                }
                Command::none()
            }
            // Cancel current operation and go to some other view.
            Msg::Cancel => {
                // Most of the time, only the last view matters. Sometimes, however, the
                // view prior to that is also considered.
                match self.view.get(0) {
                    // The user cancelled deleting a project. Return to view project.
                    Some(View::ProjectDetail(Op::Delete, id)) => {
                        self.view.push_front(View::ProjectDetail(Op::View, *id))
                    }
                    // The user cancelled creating a new Task. Return to main view
                    Some(View::TaskDetail(Op::Create, _)) => {
                        self.task = Task::default();
                        self.view.push_front(View::AllTasks);
                    }
                    // The user cancelled creating a new Project. Return to main view.
                    Some(View::ProjectDetail(Op::Create, _)) => {
                        self.task = Task::default();
                        self.view.push_front(View::AllTasks);
                    }
                    // The user cancelled editing a Project. Return to viewing that Project.
                    Some(View::ProjectDetail(Op::Edit, id)) => {
                        self.project = self.backup_project.clone();
                        self.task = Task::default();
                        self.view.push_front(View::ProjectDetail(Op::View, *id));
                    }
                    // The user can create notes from main view, project, or task. Return to
                    // previous view.
                    Some(View::NoteDetail(_, _, _, _)) => {
                        self.view.pop_front();
                    }
                    // If user clicks "Back" when viewing task, return to previous view.
                    Some(View::TaskDetail(Op::View, _)) => {
                        match self.view.get(1) {
                            Some(View::TaskDetail(Op::Edit, _)) => (),
                            _ => {
                                self.task = Task::default();
                            }
                        }
                        self.view.pop_front();
                    }
                    // If the user cancels editing a task, self.task needs set to self.backup_task
                    // (if user had been previously viewing the task), or reset task vars
                    Some(View::TaskDetail(Op::Edit, _)) => {
                        match self.view.get(1) {
                            Some(View::TaskDetail(Op::View, _)) => {
                                self.task = self.backup_task.clone();
                            }
                            _ => {
                                self.task = Task::default();
                            }
                        }
                        self.view.pop_front();
                    }
                    _ => {
                        self.view.pop_front();
                    }
                }
                Command::none()
            }
        }
    }

    fn view(&self) -> Element<Msg> {
        // Navigation by projects/tags on the left, always there.
        let nav_pane = self.navigation();

        // Mostly varying main pane in the middle, except for message to user which will
        // always be at top.
        let main_pane = match &self.view.get(0) {
            // Display main view - all tasks and controls for them
            None | Some(View::AllTasks) => {
                let new_task = new_task(self, None);

                // Get and filter tasks.
                let tasks = filter_tasks(get_tasks(self), &self.task_filter);

                // Sort tasks.
                let tasks = sort_tasks(self, tasks);

                column![
                    container(text("All Tasks").size(LARGE_FONT_SIZE)).padding([0, 0, 20, 0]),
                    new_task,
                    task_filter_controls(self),
                    task_sort_controls(self),
                    scrollable(
                        column(
                            tasks
                                .iter()
                                .map(|(id, task)| task.as_row(*id, self))
                                .collect::<Vec<_>>()
                        )
                        .spacing(8)
                    )
                ]
                .into()
            }
            Some(View::IndependentTasks) => {
                let new_task = new_task(self, None);

                // Get and filter tasks.
                let mut tasks = filter_tasks(get_tasks(self), &self.task_filter);
                tasks.retain(|_, task| task.project_id.is_none());

                // Sort tasks.
                let tasks = sort_tasks(self, tasks);

                column![
                    container(
                        text("Independent tasks (tasks under no project)").size(LARGE_FONT_SIZE)
                    )
                    .padding([0, 0, 20, 0]),
                    new_task,
                    task_filter_controls(self),
                    task_sort_controls(self),
                    scrollable(
                        column(
                            tasks
                                .iter()
                                .map(|(id, task)| task.as_row(*id, self))
                                .collect::<Vec<_>>()
                        )
                        .spacing(8)
                    )
                ]
                .into()
            }
            // Display project tasks and controls for them
            Some(View::ProjectDetail(Op::View, id)) => {
                let project = projects::get(self, *id).unwrap();

                // Get and filter tasks.
                let mut tasks = filter_tasks(get_tasks(self), &self.task_filter);
                tasks.retain(|_, task| task.project_id.is_some_and(|project_id| project_id == *id));

                // Sort tasks.
                let tasks = sort_tasks(self, tasks);

                column![
                    container(text(project.name).size(LARGE_FONT_SIZE)).padding([0, 0, 20, 0]),
                    new_task(self, Some(*id)),
                    task_filter_controls(self),
                    task_sort_controls(self),
                    scrollable(
                        column(
                            tasks
                                .iter()
                                .map(|(id, task)| task.as_row(*id, self))
                                .collect::<Vec<_>>()
                        )
                        .spacing(8)
                    )
                ]
                .into()
            }
            Some(View::ProjectDetail(Op::Create, id)) => {
                // Get and display project details.
                let name = row![text_input("Name", &self.project.name)
                    .on_input(move |new_value| {
                        Msg::TextInputChanged(Field::ProjectName, new_value)
                    })
                    .on_submit(Msg::SaveProject(*id))
                    .id(text_input::Id::new(PROJECT_NAME_INPUT_ID))];
                let controls = row![
                    button("Save").on_press(Msg::SaveProject(*id)),
                    button("Cancel").on_press(Msg::Cancel),
                ]
                .spacing(8);
                let description = row![TextInput::new("Description", &self.project.description)
                    .on_input(move |new_value| {
                        Msg::TextInputChanged(Field::ProjectDescription, new_value)
                    })
                    .on_submit(Msg::SaveProject(*id))];

                let status = if self.project.completed {
                    "Completed".to_string()
                } else {
                    "Incomplete".to_string()
                };

                let due = match self.project.due {
                    Some(v) => {
                        let dt: DateTime<Utc> =
                            DateTime::<Utc>::from_timestamp(v, 0).expect("invalid timestamp");
                        format!("{:?}", dt)
                    }
                    None => "No due date".to_string(),
                };

                let tags = if self.project.tags.is_empty() {
                    "No tags.".to_string()
                } else {
                    self.project.tags.join(", ")
                };

                column![
                    row![text("PROJECT: "), name],
                    row![text("Description:"), description]
                        .align_items(Alignment::Center)
                        .spacing(8),
                    row![text("Due:"), text(due)]
                        .align_items(Alignment::Center)
                        .spacing(8),
                    row![text("Tagged:"), text(tags)]
                        .align_items(Alignment::Center)
                        .spacing(8),
                    row![text("Priority:"), text(self.project.priority)]
                        .align_items(Alignment::Center)
                        .spacing(8),
                    row![text("Status:"), text(status)]
                        .align_items(Alignment::Center)
                        .spacing(8),
                    controls,
                ]
                .spacing(10)
                .into()
            }
            Some(View::ProjectDetail(Op::Edit, id)) => {
                // Get and display project details.
                let name = row![text_input("Name", &self.project.name)
                    .on_input(move |new_value| {
                        Msg::TextInputChanged(Field::ProjectName, new_value)
                    })
                    .on_submit(Msg::SaveProject(*id))
                    .id(text_input::Id::new(PROJECT_NAME_INPUT_ID))];
                let controls = row![
                    button("Save").on_press(Msg::SaveProject(*id)),
                    button("Cancel").on_press(Msg::Cancel),
                ]
                .spacing(8);
                let description = row![TextInput::new("Description", &self.project.description)
                    .on_input(move |new_value| {
                        Msg::TextInputChanged(Field::ProjectDescription, new_value)
                    })
                    .on_submit(Msg::SaveProject(*id))];

                let status = if self.project.completed {
                    "Completed".to_string()
                } else {
                    "Incomplete".to_string()
                };

                let due = match self.project.due {
                    Some(v) => {
                        let dt: DateTime<Utc> =
                            DateTime::<Utc>::from_timestamp(v, 0).expect("invalid timestamp");
                        format!("{:?}", dt)
                    }
                    None => "No due date".to_string(),
                };

                let tags = if self.project.tags.is_empty() {
                    "No tags.".to_string()
                } else {
                    self.project.tags.join(", ")
                };

                column![
                    row![text("PROJECT: "), name],
                    row![text("Description:"), description]
                        .align_items(Alignment::Center)
                        .spacing(8),
                    row![text("Due:"), text(due)]
                        .align_items(Alignment::Center)
                        .spacing(8),
                    row![text("Tagged:"), text(tags)]
                        .align_items(Alignment::Center)
                        .spacing(8),
                    row![text("Priority:"), text(self.project.priority)]
                        .align_items(Alignment::Center)
                        .spacing(8),
                    row![text("Status:"), text(status)]
                        .align_items(Alignment::Center)
                        .spacing(8),
                    controls,
                ]
                .spacing(10)
                .into()
            }
            Some(View::ProjectDetail(Op::Delete, id)) => {
                let project = projects::get(self, *id).unwrap();
                column![
                text(format!(
                    r#"Are you sure you want to delete the project "{}"? All notes and tasks associated with it will also be deleted."#,
                    project.name
                )),
                row![
                    button("Yes").on_press(Msg::DeleteProject(*id)),
                    button("No").on_press(Msg::Cancel),
                ]
            ]
            .into()
            }
            Some(View::TaskDetail(op, id)) => task_detail(self, op, *id, self.task.project_id),
            Some(View::NoteDetail(_, note_type, id, parent_id)) => {
                match notes::get(self, *id) {
                    // Editing existing note
                    Some(_) => note_detail(self, *note_type, *id, *parent_id),
                    // Creating new note
                    None => note_detail(self, *note_type, *id, *parent_id),
                }
            }
        };

        // Varying things at the right - project/task details and notes
        let right_pane = match self.view.get(0) {
            Some(View::AllTasks) => {
                let mut notes = notes::get_notes(self, None, None);

                // Potentially filter notes by notetype,
                // and also filter out any task-level note whose task is completed.
                match self.note_filter {
                    None => {
                        // Keep all notes except task notes whose task is completed.
                        notes.retain(|_, note| {
                            if note.note_type == NoteType::Task {
                                let task = tasks::get(self, note.parent_id.unwrap()).unwrap();
                                !task.completed
                            } else {
                                true
                            }
                        });
                    }
                    Some(NoteType::Task) => {
                        // Keep all task notes, except for task notes whose task is completed
                        notes.retain(|_, note| {
                            if note.note_type == NoteType::Task {
                                let task = tasks::get(self, note.parent_id.unwrap()).unwrap();
                                !task.completed
                            } else {
                                false
                            }
                        });
                    }
                    Some(v) => {
                        // Keep only notes of this kind (App or Project).
                        notes.retain(|_, note| note.note_type == v);
                    }
                }

                // Convert to vector to sort
                let mut notes: Vec<(Uuid, Note)> = notes.into_iter().collect();

                // A saved note always has a Some value for `created`, so unwrap is ok.
                notes.sort_by_key(|(_, note)| -note.created.unwrap());

                let notes = if notes.is_empty() {
                    column!["No notes."]
                } else {
                    column(
                        notes
                            .iter()
                            .map(|(id, note)| note.as_row(*id, self))
                            .collect::<Vec<_>>(),
                    )
                    .spacing(8)
                };

                column![
                    row![
                        text("Notes").size(LARGE_FONT_SIZE),
                        text("(excluding notes from completed tasks)"),
                    ]
                    .align_items(Alignment::End)
                    .spacing(10),
                    button("Create new note").on_press(Msg::CreateNote(NoteType::App, None)),
                    note_filter_controls(self),
                    scrollable(notes),
                ]
                .spacing(10)
            }
            Some(View::ProjectDetail(Op::View, id)) => {
                let notes = notes::get_notes(self, Some(NoteType::Project), Some(*id));

                // Convert to vector to sort.
                let mut notes: Vec<(Uuid, Note)> = notes.into_iter().collect();
                // A saved note always has a Some value for `created`, so unwrap is ok.
                notes.sort_by_key(|(_, note)| -note.created.unwrap());

                let notes = if notes.is_empty() {
                    column!["No notes."]
                } else {
                    column(
                        notes
                            .iter()
                            .map(|(id, note)| note.as_row(*id, self))
                            .collect::<Vec<_>>(),
                    )
                    .spacing(8)
                };

                column![
                    project_meta(self, *id),
                    project_controls(*id),
                    scrollable(notes)
                ]
            }
            Some(View::ProjectDetail(Op::Create, _)) | Some(View::ProjectDetail(Op::Edit, _)) => {
                column![]
            }
            // Display the project meta info (if this task belongs to a project) and task notes.
            Some(View::TaskDetail(_, id)) => {
                let notes = notes::get_notes(self, Some(NoteType::Task), Some(*id));

                // Convert to vector to sort.
                let mut notes: Vec<(Uuid, Note)> = notes.into_iter().collect();
                // A saved note always has a Some value for `created`, so unwrap is ok.
                notes.sort_by_key(|(_, note)| -note.created.unwrap());

                let notes = if notes.is_empty() {
                    column!["No notes."]
                } else {
                    column(
                        notes
                            .iter()
                            .map(|(id, note)| note.as_row(*id, self))
                            .collect::<Vec<_>>(),
                    )
                    .spacing(8)
                };

                if let Some(v) = self.task.project_id {
                    column![project_meta(self, v), text("Task Notes"), scrollable(notes)]
                        .spacing(10)
                } else {
                    column![text("Task Notes"), scrollable(notes)]
                }
            }
            // Display parent (project and/or task) details for context.
            Some(View::NoteDetail(_, note_type, _, parent_id)) => {
                if let Some(v) = parent_id {
                    match note_type {
                        NoteType::Project => {
                            let project = projects::get(self, *v).unwrap();
                            column![row![
                                text("Project: "),
                                button(text(&project.name))
                                    .on_press(Msg::ViewProject(*v))
                                    .style(theme::Button::Text)
                            ]
                            .align_items(Alignment::Center)]
                        }
                        NoteType::Task => {
                            let task = tasks::get(self, *v).unwrap();
                            // check if that task has a parent to also display
                            if let Some(task_parent) = task.project_id {
                                let project = projects::get(self, task_parent).unwrap();
                                column![
                                    row![
                                        text("Project: "),
                                        button(text(&project.name))
                                            .on_press(Msg::ViewProject(task_parent))
                                            .style(theme::Button::Text)
                                    ]
                                    .align_items(Alignment::Center),
                                    row![
                                        text("Task: "),
                                        button(text(&task.name))
                                            .on_press(Msg::ViewTask(*v))
                                            .style(theme::Button::Text)
                                    ]
                                    .align_items(Alignment::Center)
                                ]
                            } else {
                                column![row![
                                    text("Task: "),
                                    button(text(&task.name))
                                        .on_press(Msg::ViewTask(*v))
                                        .style(theme::Button::Text)
                                ]
                                .align_items(Alignment::Center)]
                            }
                        }
                        NoteType::App => column![],
                    }
                } else {
                    column![]
                }
            }
            _ => column![],
        };

        row![
            scrollable(nav_pane).width(Length::FillPortion(1)),
            vertical_rule(20).style(CustomColorVerticalRule::new(color!(ORANGE)).style()),
            column![
                text(self.message_to_user.clone()),
                row![
                    container(main_pane)
                        .padding(10)
                        .width(Length::FillPortion(1)),
                    container(right_pane).width(Length::FillPortion(1)),
                ]
                .spacing(20)
                .align_items(iced::Alignment::Start)
            ]
            .width(Length::FillPortion(6))
        ]
        .spacing(10)
        .padding(10)
        .into()
    }

    fn theme(&self) -> Theme {
        styles::custom_theme()
    }

    fn title(&self) -> String {
        match self.view.get(0) {
            None => String::from("Projects and Tasks"),
            Some(v) => v.title(),
        }
    }
}

impl App {
    /// Reconcile any changes and save doc to file.
    fn write_to_file(&mut self, doc: TaskDoc) {
        reconcile(&mut self.doc, doc).unwrap();
        let mut f = OpenOptions::new()
            .write(true)
            .open("tasks")
            .expect("Could not open file.");
        f.write_all(&self.doc.save()).unwrap();

        // also save a json version (during development)
        let json_file = OpenOptions::new()
            .write(true)
            .create(true)
            .open("tasks.json")
            .expect("Could not open file.");
        serde_json::to_writer_pretty(json_file, &AutoSerde::from(&self.doc)).unwrap();
    }

    /// Display navigation.
    fn navigation(&self) -> Element<Msg> {
        let home = button(text("Home").size(MEDIUM_FONT_SIZE))
            .on_press(Msg::Home)
            .style(theme::Button::Text)
            .padding(0);

        let new_project = match self.view.get(0) {
            Some(View::ProjectDetail(Op::Create, _)) => button("New"), // disabled
            _ => button(text("New").size(SMALL_FONT_SIZE)).on_press(Msg::CreateProject),
        };

        // List all projects
        let projects = get_projects(self);
        let projects = if projects.is_empty() {
            column!["No projects yet!"]
        } else {
            // Convert to vector to sort
            let mut projects: Vec<(Uuid, Project)> = projects.into_iter().collect();
            projects.sort_by_key(|(_, project)| project.name.clone());
            column(
                projects
                    .iter()
                    .map(|(id, project)| {
                        button(text(format!("- {}", project.name.clone())))
                            .on_press(Msg::ViewProject(*id))
                            .style(theme::Button::Text)
                            .padding([0, 0, 0, 10])
                            .into()
                    })
                    .collect::<Vec<_>>(),
            )
            .spacing(10)
        };

        let independent_tasks = button(text("Independent Tasks").size(MEDIUM_FONT_SIZE))
            .on_press(Msg::IndependentTasks)
            .style(theme::Button::Text)
            .padding(0);

        column![
            container(home).padding([10, 0, 20, 0]),
            row![text("Projects").size(MEDIUM_FONT_SIZE), new_project].spacing(12),
            projects,
            container(independent_tasks).padding([20, 0, 0, 0]),
        ]
        .spacing(10)
        .into()
    }
}

#[derive(Debug, Clone, Reconcile, Hydrate, PartialEq)]
struct TaskDoc {
    #[autosurgeon(with = "autosurgeon::map_with_parseable_keys")]
    projects: HashMap<Uuid, Project>,
    #[autosurgeon(with = "autosurgeon::map_with_parseable_keys")]
    tasks: HashMap<Uuid, Task>,
    #[autosurgeon(with = "autosurgeon::map_with_parseable_keys")]
    notes: HashMap<Uuid, Note>,
}
