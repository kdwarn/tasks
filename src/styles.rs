use iced::{
    color, theme,
    widget::rule::{self, FillMode},
    BorderRadius, Color, Theme,
};

pub const TEXT_COLOR: i32 = 0xded0ba;
pub const ORANGE: i32 = 0x6b5124;
pub const DARK_GRAY: i32 = 0x333333;
pub const SMALL_FONT_SIZE: f32 = 14.0;
pub const MEDIUM_FONT_SIZE: f32 = 18.0;
pub const LARGE_FONT_SIZE: f32 = 20.0;

// Using the limited (but useful!) custom theme options.
pub fn custom_theme() -> Theme {
    Theme::custom(theme::Palette {
        background: color!(DARK_GRAY),
        text: color!(TEXT_COLOR),
        primary: color!(ORANGE),
        success: Color::from_rgb(0.0, 1.0, 0.0),
        danger: Color::from_rgb(1.0, 0.0, 0.0),
    })
}

pub struct CustomColorVerticalRule {
    color: Color,
}

impl CustomColorVerticalRule {
    pub fn new(color: Color) -> Self {
        Self { color }
    }

    pub fn style(self) -> theme::Rule {
        theme::Rule::Custom(Box::new(self))
    }
}

impl rule::StyleSheet for CustomColorVerticalRule {
    type Style = Theme;

    fn appearance(&self, _: &Self::Style) -> rule::Appearance {
        rule::Appearance {
            color: self.color,
            width: 2,
            radius: BorderRadius::default(),
            fill_mode: FillMode::Full,
        }
    }
}
