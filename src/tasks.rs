use std::collections::HashMap;
use std::fmt::{self, Display, Formatter};

use autosurgeon::{hydrate, Hydrate, Reconcile};
use chrono::{DateTime, Utc};
use iced::{
    theme,
    widget::{
        button, checkbox, column, container, pick_list, row, text, text_input, Button, Checkbox,
    },
    Alignment, Color, Element, Length,
};
use uuid::Uuid;

use crate::{
    notes::NoteType, projects, App, Field, Msg, Op, TaskDoc, View, SMALL_FONT_SIZE,
    TASK_NAME_INPUT_ID,
};

#[derive(Debug, Clone, Reconcile, Hydrate, PartialEq)]
pub struct Task {
    pub project_id: Option<Uuid>,
    pub name: String,
    pub due: Option<i64>,
    pub description: String,
    pub priority: Priority,
    pub completed: bool,
    pub tags: Vec<String>,
    pub created: Option<i64>,
    pub edited: Option<i64>,
}

impl Default for Task {
    fn default() -> Self {
        Self {
            project_id: None,
            name: String::new(),
            due: None,
            description: String::new(),
            tags: vec![],
            priority: Priority::Normal,
            completed: false,
            created: None,
            edited: None,
        }
    }
}

impl Display for Task {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", &self.name)
    }
}

impl Task {
    /// Display a task as a row in a list of tasks, with various controls and details.
    pub fn as_row<'a>(&self, id: Uuid, app: &App) -> Element<'a, Msg> {
        let complete_button: Button<Msg> = if self.completed {
            button("Mark as Incomplete").on_press(Msg::ChangeCompletedStatus(id, false))
        } else {
            button("Mark as Complete").on_press(Msg::ChangeCompletedStatus(id, true))
        };
        let checkbox: Checkbox<Msg> = checkbox("", false, |_| Msg::SelectTask);

        let name = button(text(self.name.clone()))
            .on_press(Msg::ViewTask(id))
            .style(theme::Button::Text)
            .padding(0);

        let edit_button = button("Edit").on_press(Msg::EditTask(id));
        let delete_button = button("Delete").on_press(Msg::DeleteTask(id));
        // If on AllTasks view, include name of project this is from, if any.
        let task_text = if let Some(View::AllTasks) = app.view.get(0) {
            match self.project_id {
                Some(v) => match projects::get(app, v) {
                    Some(project) => {
                        column![name, text(&format!("{project}")).size(SMALL_FONT_SIZE)]
                            .align_items(Alignment::Start)
                    }
                    None => column![name],
                },
                None => column![name],
            }
        } else {
            column![name]
        };

        row![
            row![checkbox, task_text].width(Length::Fill),
            row![edit_button, delete_button, complete_button].spacing(8),
        ]
        .spacing(10)
        .into()
    }
}

#[derive(Debug, Default, Clone, Copy, Reconcile, Hydrate, PartialEq, Eq, PartialOrd, Ord)]
pub enum Priority {
    High,
    #[default]
    Normal,
    Low,
}

impl Priority {
    const ALL: [Priority; 3] = [Priority::High, Priority::Normal, Priority::Low];
}

impl Display for Priority {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let text = match self {
            Priority::High => "High",
            Priority::Normal => "Normal",
            Priority::Low => "Low",
        };
        write!(f, "{text}")
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum TaskFilter {
    All,
    Incomplete,
    Completed,
}

// Field to sort tasks by.
#[derive(Debug, Copy, Clone, PartialEq)]
#[non_exhaustive]
pub enum TaskSort {
    Priority,
    Created,
    Due,
}

/// Get tasks.
pub fn get_tasks(app: &App) -> HashMap<Uuid, Task> {
    let doc: TaskDoc = hydrate(&app.doc).unwrap();
    doc.tasks
}

/// Get one task.
pub fn get(app: &App, id: Uuid) -> Option<Task> {
    let doc: TaskDoc = hydrate(&app.doc).unwrap();
    doc.tasks.get(&id).cloned()
}

/// Create, view, or edit task.
pub fn task_detail<'a>(app: &App, op: &Op, id: Uuid, project_id: Option<Uuid>) -> Element<'a, Msg> {
    let name = match op {
        Op::View => row![text(&app.task.name)],
        _ => row![
            text("Name:"),
            text_input("Name", &app.task.name)
                .on_input(move |new_value| { Msg::TextInputChanged(Field::TaskName, new_value) })
                .on_submit(Msg::SaveTask(id, project_id))
                .id(text_input::Id::new(TASK_NAME_INPUT_ID))
        ],
    };

    let description = match op {
        Op::View => row![text(&app.task.description)],
        _ => row![
            text("Description"),
            text_input("Description", &app.task.description)
                .on_input(move |new_value| {
                    Msg::TextInputChanged(Field::TaskDescription, new_value)
                })
                .on_submit(Msg::SaveTask(id, project_id))
        ],
    };

    let status = if app.task.completed {
        "Completed".to_string()
    } else {
        "Incomplete".to_string()
    };

    let due = match app.task.due {
        Some(v) => {
            let dt: DateTime<Utc> =
                DateTime::<Utc>::from_timestamp(v, 0).expect("invalid timestamp");
            format!("{:?}", dt)
        }
        None => "No due date".to_string(),
    };

    let priority = match op {
        Op::View => row![text("Priority: "), text(app.task.priority)],
        _ => row![pick_list(
            &Priority::ALL[..],
            Some(app.task.priority),
            Msg::ChangePriority,
        )
        .placeholder("Priority...")],
    };

    let tags = if app.task.tags.is_empty() {
        "No tags".to_string()
    } else {
        app.task.tags.join(", ")
    };

    let task_details = column![
        name,
        description,
        row![text("Due:"), text(due)]
            .align_items(Alignment::Center)
            .spacing(8),
        priority,
        row![text("Tagged:"), text(tags)]
            .align_items(Alignment::Center)
            .spacing(8),
        row![text("Status:"), text(status)]
            .align_items(Alignment::Center)
            .spacing(8),
    ]
    .spacing(10);

    let controls = match op {
        Op::View => {
            let complete_button = if app.task.completed {
                button("Mark as Incomplete").on_press(Msg::ChangeCompletedStatus(id, false))
            } else {
                button("Mark as Complete").on_press(Msg::ChangeCompletedStatus(id, true))
            };
            row![
                button("Back").on_press(Msg::Cancel),
                button("Edit").on_press(Msg::EditTask(id)),
                complete_button,
                button("Delete").on_press(Msg::DeleteTask(id)),
                button("Add Note").on_press(Msg::CreateNote(NoteType::Task, Some(id)))
            ]
        }
        Op::Edit => {
            let complete_button = if app.task.completed {
                button("Mark as Incomplete").on_press(Msg::ChangeCompletedStatus(id, false))
            } else {
                button("Mark as Complete").on_press(Msg::ChangeCompletedStatus(id, true))
            };
            row![
                button("Save").on_press(Msg::SaveTask(id, project_id)),
                complete_button,
                button("Cancel").on_press(Msg::Cancel),
            ]
        }
        Op::Create => {
            row![
                button("Save").on_press(Msg::SaveTask(id, project_id)),
                button("Cancel").on_press(Msg::Cancel),
            ]
        }
        Op::Delete => row![],
    };

    container(
        column![
            row![text("TASK"), controls.spacing(8)]
                .spacing(8)
                .align_items(Alignment::Center),
            task_details,
        ]
        .spacing(10),
    )
    .padding(10)
    .into()
}

/// Input to create new task.
pub fn new_task<'a>(app: &App, project_id: Option<Uuid>) -> Element<'a, Msg> {
    // Create input for creating a new task.
    text_input("Create new task", &app.task.name)
        .on_input(move |new_value| Msg::TextInputChanged(Field::TaskName, new_value))
        .on_submit(Msg::SaveTask(Uuid::new_v4(), project_id))
        .id(text_input::Id::new(TASK_NAME_INPUT_ID))
        .into()
}

/// Create a set of controls to filter tasks.
pub fn task_filter_controls(app: &App) -> Element<Msg> {
    // this is a slightly modified version of main_controls() from Iced's todos example
    let filter_button = |label, filter| {
        let label = text(label);

        let button = button(label).style(if filter == app.task_filter {
            theme::Button::Primary
        } else {
            theme::Button::Secondary
        });

        button.on_press(Msg::FilterTasks(filter)).padding(8)
    };

    row![
        text("Filter by status:").style(Color::from_rgb(0.5, 0.5, 0.5)),
        filter_button("All", TaskFilter::All),
        filter_button("Incomplete", TaskFilter::Incomplete),
        filter_button("Completed", TaskFilter::Completed),
    ]
    .width(Length::Shrink)
    .spacing(10)
    .padding(10)
    .align_items(Alignment::Center)
    .into()
}

pub fn filter_tasks(tasks: HashMap<Uuid, Task>, filter: &TaskFilter) -> HashMap<Uuid, Task> {
    match filter {
        TaskFilter::All => tasks,
        TaskFilter::Incomplete => tasks
            .into_iter()
            .filter(|(_, task)| !task.completed)
            .collect(),
        TaskFilter::Completed => tasks
            .into_iter()
            .filter(|(_, task)| task.completed)
            .collect(),
    }
}

/// Create a set of controls to sort tasks.
pub fn task_sort_controls(app: &App) -> Element<Msg> {
    let sort_button = |label, sort| {
        let label = text(label);

        let button = button(label).style(if sort == app.task_sort {
            theme::Button::Primary
        } else {
            theme::Button::Secondary
        });

        button.on_press(Msg::SortTasks(sort)).padding(8)
    };

    row![
        text("Sort by:").style(Color::from_rgb(0.5, 0.5, 0.5)),
        sort_button("Priority", TaskSort::Priority),
        sort_button("Due time", TaskSort::Due),
        sort_button("Created time", TaskSort::Created),
    ]
    .width(Length::Shrink)
    .spacing(10)
    .padding(10)
    .align_items(Alignment::Center)
    .into()
}

pub fn sort_tasks(app: &App, tasks: HashMap<Uuid, Task>) -> Vec<(Uuid, Task)> {
    // Convert to Vec to sort.
    let mut tasks = tasks.into_iter().collect::<Vec<_>>();

    // First sort by due and created dates, so that those with same values otherwise
    // stay in same order in list.
    tasks.sort_by_key(|(_, task)| task.due);
    tasks.sort_by_key(|(_, task)| -task.created.unwrap());

    match app.task_sort {
        TaskSort::Priority => tasks.sort_by_key(|(_, task)| task.priority),
        TaskSort::Due => tasks.sort_by_key(|(_, task)| task.due),
        TaskSort::Created => (), // already done above
    };
    tasks
}
