use std::collections::HashMap;

use autosurgeon::{hydrate, Hydrate, Reconcile};
use iced::{
    theme,
    widget::{button, checkbox, column, container, row, text, text_editor, Checkbox},
    Alignment, Color, Element, Length,
};
use uuid::Uuid;

use crate::{projects, tasks, App, Msg, TaskDoc, View, SMALL_FONT_SIZE};

#[derive(Debug, Clone, Reconcile, Hydrate, PartialEq)]
pub struct Note {
    pub parent_id: Option<Uuid>, // possible Project or Task (None for App)
    pub note_type: NoteType,
    pub text: String,
    pub created: Option<i64>,
    pub edited: Option<i64>,
}

impl Default for Note {
    fn default() -> Self {
        Self {
            parent_id: None,
            note_type: NoteType::App, // start with this, change as necessary
            text: String::new(),
            created: None,
            edited: None,
        }
    }
}

// What type of struct the note applies to
#[derive(Debug, Copy, Clone, Hydrate, Reconcile, PartialEq, PartialOrd)]
pub enum NoteType {
    App,
    Project,
    Task,
}

impl Note {
    /// Display a note as a row in a list of notes, with various controls and details.
    pub fn as_row<'a>(&self, id: Uuid, app: &App) -> Element<'a, Msg> {
        // Add a set of controls to each one and build a display out of them.
        let checkbox: Checkbox<Msg> = checkbox("", false, |_| Msg::SelectNote);
        let edit_button = button("Edit").on_press(Msg::EditNote(id, self.clone()));
        let delete_button = button("Delete").on_press(Msg::DeleteNote(id));
        // Potentially include project or task name this note is from, if any.
        let note_text = text(self.text.clone());

        // If on AllTasks view, the project this is under, if any, should be shown.
        let project = if let Some(View::AllTasks) = app.view.get(0) {
            match self.note_type {
                NoteType::Project => projects::get(app, self.parent_id.unwrap()),
                NoteType::Task => {
                    let task = tasks::get(app, self.parent_id.unwrap()).unwrap();
                    if let Some(v) = task.project_id {
                        projects::get(app, v)
                    } else {
                        None
                    }
                }
                NoteType::App => None,
            }
        } else {
            None
        };

        let note_text = if let Some(v) = project {
            column![note_text, text(v.name).size(SMALL_FONT_SIZE)].align_items(Alignment::Start)
        } else {
            column![note_text]
        };

        row![
            row![checkbox, note_text].width(Length::Fill),
            row![edit_button, delete_button].spacing(8),
        ]
        .spacing(10)
        .into()
    }
}

/// Get all notes.
pub fn get_notes(
    app: &App,
    note_type: Option<NoteType>,
    parent_id: Option<Uuid>,
) -> HashMap<Uuid, Note> {
    let doc: TaskDoc = hydrate(&app.doc).unwrap();
    let mut notes = doc.notes;

    match note_type {
        Some(NoteType::App) => {
            notes.retain(|_, note| note.note_type == NoteType::App);
        }
        Some(NoteType::Project) => {
            notes.retain(|_, note| {
                note.note_type == NoteType::Project && note.parent_id == parent_id
            });
        }
        Some(NoteType::Task) => {
            notes.retain(|_, note| note.note_type == NoteType::Task && note.parent_id == parent_id);
        }
        None => (),
    }
    notes
}

/// Get one note.
pub fn get(app: &App, id: Uuid) -> Option<Note> {
    let doc: TaskDoc = hydrate(&app.doc).unwrap();
    doc.notes.get(&id).cloned()
}

/// Create or edit note (there is no view of individual note - only list).
pub fn note_detail(
    app: &App,
    note_type: NoteType,
    id: Uuid,
    parent_id: Option<Uuid>,
) -> Element<Msg> {
    let note_text_editor =
        container(text_editor(&app.note_text).on_action(Msg::ChangeNoteText)).padding(10);

    let controls = row![
        button("Save").on_press(Msg::SaveNote(note_type, id, parent_id)),
        button("Cancel").on_press(Msg::Cancel),
    ]
    .align_items(Alignment::Center)
    .spacing(8);

    column![controls, note_text_editor]
        .align_items(Alignment::Start)
        .spacing(10)
        .into()
}

/// Create a set of controls to filter notes.
pub fn note_filter_controls<'a>(app: &App) -> Element<'a, Msg> {
    let filter_button = |label, filter| {
        let label = text(label);

        let button = button(label).style(if app.note_filter == filter {
            theme::Button::Primary
        } else {
            theme::Button::Secondary
        });

        button.on_press(Msg::FilterNotes(filter)).padding(8)
    };

    row![
        text("Filter:").style(Color::from_rgb(0.5, 0.5, 0.5)),
        filter_button("All", None),
        filter_button("App", Some(NoteType::App)),
        filter_button("Project", Some(NoteType::Project)),
        filter_button("Task", Some(NoteType::Task)),
    ]
    .width(Length::Shrink)
    .spacing(10)
    .padding(10)
    .align_items(Alignment::Center)
    .into()
}
